"""
PYTHON MODULE:
    seal_model.py
PURPOSE:
    Module contains cell class for seal model computations.
REVISION HISTORY:
    2019-04-11  Started work on this module.
    2019-06-10  Beta established.
    2020-01-31  Beta update.
    2020-03-05  Beta Version 1.3.
CLASS FUNCTIONS IN MODULE (9):
    ** Class Cell ****
    __init__(self, cell_area, cell_thickness, cell_permeability)
    set_depth(self, repository_depth)
    set_coord(self, x_value, y_value)
    get_value(self, select)
    xx print_cell(self) - NOT USED - DEBUGGING
    compute_static_pressure(self)
    compute_model_influence(self, velocity)
    compute_flow(self, base_co2_pressure, base_co2_saturation,
                 top_brine_pressure, seal_controls)
    track_history(self, base_co2_pressure, co2_flow_rate, time_step)
CLASS METHOD (1):
    assign_controls(cls, seal_controls):
OTHER (1):
    convert_flows(co2_flow, brine_flow, current_time, past_time, cols)
-------------------------------------------------------------------------------
                            Warranty Disclaimer
  This software is provided for use as-is and no representations about the
  suitability of this software is made. It is provided without warranty.

  This software development was funded by the Department of Energy, National
  Energy Technology Laboratory, an agency of the United States Government,
  through a support contract with Battelle Memorial Institute as part of the
  Leidos Research Support Team (LRST). Neither the United States Government
  nor any agency thereof, nor any of their employees, nor LRST nor any of
  their employees, makes any warranty, express or implied, or assumes any
  legal liability or responsibility for the accuracy, completeness, or
  usefulness of any information, product, software or process disclosed,
  or represents that its use would not infringe privately owned rights.
-------------------------------------------------------------------------------
                            Acknowledgment
  This software and its documentation were completed as part of National
  Risk Assessment Partnership project. Support for this project came from
  the U.S. Department of Energy's (DOE) Office of Fossil Energy's
  Crosscutting Research program. The technical effort was performed in
  support of the DOE National Energy Technology Laboratory's ongoing
  research by the Leidos Research Support Team (LRST) under the Research
  Support Services contract 89243318CFE000003.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
  Copyright(c) 2019-2020 by Ernest N. Lindner - All Rights Reserved
-------------------------------------------------------------------------------
"""
import logging

import seal_units as sunit      # For unit conversion
import seal_perm as perm        # for permeability definitions/computations

# Other constants
V_SMALL_SAT = 1.0e-02           # small saturation - start solubility option

logging.basicConfig(format='\n  --> %(levelname)s: %(message)s',
                    level=logging.WARNING)



class Cell():
    """ A group of seal areal cells above the injection horizon. """

    # Class constants with default values.
    #  NOTE: Names are NOT capitalized as these will added within IAM.

    # Fluid parameters (Could differ for each cell)
    brineDensity = 1007.0       # kg/m^3
    co2Density = 582.6          # kg/m^3
    brineViscosity = 5.596e-4   # Pa*s
    co2Viscosity = 4.387e-5     # Pa*s
    co2Solubility = 2.0E-03     # kg/kg

    # Two-phase model parameters for relative permeability
    relativeModel = 'BC'        # Relative permeability model
    brineResSaturation = 0.1    # (--)
    co2ResSaturation = 0.0      # (--)

    # Time-model parameters (Could differ for each cell)
    model = 0                   # Model type 0/1/2
    totalEffect = 0.5           # Final Perm = 50% of total
    rateEffect = 0.1            # Time function in years
    flowMinimum = 1.0E-09       # Minimum flow limit for aging
    reactivity = 8.0            # Value: 1 to 10
    carbonateContent = 5        # Shale
    clayContent = 60.0          # Clay - for a typical shale
    clayType = "smectite"       # clay type
                                #  -> with ["smectite", "illite", "chlorite"]

    def __init__(self, x_center=0.0, y_center=0.0):
        """Initialize attributes of a flow cell as part of areal grid.
        Input Variables:
            x_center = x-coordinate of center (m)
            y_center = y-coordinate of center (m)
        Default Values:
            area = horizontal area of cell (m^2)
            thickness = vertical thickness of cell (m)
            depth = depth to top of cell (m)
            status = status of cell - active=1; not used=0
            permeability = total vertical permeability (microdarcys)
            history = exposure time (yrs)
            influence = permeability factor for exposure (1.0 = None)
            entry = threshold pressure (Pa)
        """
        self.x_center = x_center
        self.y_center = y_center
        self.area = 10000.0             # area of cell (m2)
        self.thickness = 100.0          # thickness of cell (m)
        self.depth = 1100               # depth to <top> of cell (m)
        self.status = int(1)            # created as active (=1)
        self.permeability = 1.0         # total perm. (microdarcys)
        self.history = 0.0              # exposure history (yrs)
        self.influence = 1.0            # factor on permeability
        self.entry = 5000.0             # entry pressure for cell (Pa)

    def set_depth(self, repository_depth):
        """ Define depth to "top" of a cell.
        Input Variable:
            repository_depth = depth to top of repository (m)
        Note:
            Define thickness of cell first.
        """
        self.depth = repository_depth - self.thickness

    def set_coord(self, x_value, y_value):
        """ Define new influence value for analysis.
        Input Variable:
            x_value = x coordinate
            y_value = y coordinate
        """
        self.x_center = x_value
        self.y_center = y_value

    def get_value(self, select):
        """ Return a parameter value of a cell depending on code.
        Input Variable:
            select = parameter number code
        Return:
            result
        """
        if select == 0:
            result = self.permeability
        elif select == 1:
            result = self.thickness
        elif select == 2:
            result = self.influence
        elif select == 3:
            result = self.area
        elif select == 4:
            result = self.depth
        elif select == 5:
            result = self.status
        elif select == 6:
            result = self.x_center
        elif select == 7:
            result = self.y_center
        elif select == 8:
            result = self.entry
        elif select == 9:
            result = self.history
        else:
            result = "ERROR!!"

        return result

    #def print_cell(self):
    #    """ Print instance properties of a cell for Debugging.
    #    Input Variables:
    #        marker = current cell/instance number
    #    Print Variables:
    #        area = area for flow (m2)
    #        thickness = vertical thickness (m)
    #        permeability = total vertical perm. (in microdarcys)
    #        status = status of cell - active (=1) or not used (=0)
    #        history = exposure time (yrs)
    #        influence = permeability factor for exposure (--)
    #        x_center = x-coordinate of center (m)
    #        y_center = y-coordinate of center (m)
    #        depth = depth to top of cell (m)
    #    Return:
    #        None
    #    """
    #    print("\n The Cell Parameters are:")
    #    print("   Area = {0} m2".format(self.area))
    #    print("   Thickness = {0} m".format(self.thickness))
    #    print("   Permeability =  {0:7.4e} microdarcys".format(self.permeability))
    #    print("   Status = {0}".format(self.status))
    #    print("   History = {0} yrs".format(self.history))
    #    print("   Influence = {0}".format(self.influence))
    #    print("   X Coordinate = {0} m".format(self.x_center))
    #    print("   Y Coordinate = {0} m".format(self.y_center))
    #    print("   Depth to top of cell = {0} m".format(self.depth))
    #    print("   Entry Pressure = {0} Pa".format(self.entry))
    #
    #    # return None


    def compute_static_pressure(self, reference_pressure, reference_depth):
        """ Compute static pressure at cell top.
        Input Variable:
            reference_pressure  reference pressure (Pa)
            reference_depth = reference depth (m)
        Return:
            pressure = static pressure at top of cell (Pa)
        """
        depth_change = self.depth - reference_depth
        pressure_change = self.brineDensity * depth_change * sunit.gravity()
        pressure = reference_pressure + pressure_change

        return pressure

    def compute_model_influence(self, velocity):
        """ Compute influence of alteration on permeability.
        Input Variables:
            velocity = CO2 flow velocity (m/s)
        Return:
            None
        """
        # Update only for active cells.
        if self.status > 0:
            if self.model == 1:          # Time model.
                # Change influence factor based on time alone.
                self.influence = perm.model_z_analysis(self.totalEffect,
                                                       self.rateEffect,
                                                       self.history)
            elif self.model == 2:        # Multi-variant model.
                prior = self.influence
                # Compute effects due to constitutive model on mineralogy.
                change_factor = perm.mineral_factor(velocity,
                                                    self.reactivity,
                                                    self.clayContent,
                                                    self.clayType,
                                                    self.carbonateContent)

                # compute z-factor from current time.
                new_influence = perm.model_z_analysis(change_factor,
                                                      self.rateEffect,
                                                      self.history)

                # Compute incremental change to influence.
                self.influence += new_influence - prior

            else:
                # No model used; no change to factor.
                pass
        else:
            # cell inactive; no change to influence.
            pass

        # return None

    def compute_flow(self, base_co2_pressure, base_co2_saturation,
                     top_brine_pressure, seal_controls):
        """ Compute CO2 flow rate through a cell.
        Input Variables:
            base_co2_pressure = CO2 pressure at base of cell (Pa)
            base_co2_saturation = CO2 saturation at base of cell (--)
            top_brine_pressure = brine pressure at top of cell (Pa)
            seal_controls = dictionary of parameters
         Return:
            co2_flow = CO2 flux (rate - kg/sec)
            brine_flow = brine flux (rate - kg/sec)
        """
        #-----------------------------------------------------------------------
        # Work only on active cells
        if self.status == 1:
            # CO2:
            # -> Only pressure is large enough.
            if base_co2_pressure > self.entry:

                # Compute the effective wet saturation for cell.
                effective_saturation = perm.compute_effective_saturation(
                    base_co2_saturation, self.brineResSaturation,
                    self.co2ResSaturation)

                # Compute the capillary pressure at top of seal.
                capillary_press = perm.compute_capillary_pressure(
                    self.relativeModel, effective_saturation, seal_controls,
                    self.entry)

                # Get the CO2 relative permeability factor for the cell.
                relative_perm = \
                    perm.co2_relative_permeability(self.relativeModel,
                                                   effective_saturation,
                                                   seal_controls)

                # Compute the CO2 pressure at top of seal.
                top_pressure = capillary_press + top_brine_pressure

                # Get CO2 effective vertical permeability in (m2) for cell.
                effective_permeability = perm.compute_current_permeability(
                    relative_perm, self.influence, self.permeability)

                # Compute volume flux (m3/s) for cell:
                pressure_head = (base_co2_pressure - top_pressure) / self.thickness
                hydraulic_head = sunit.gravity() * self.co2Density
                co2_flow = (self.area * effective_permeability
                            * (pressure_head - hydraulic_head)
                            / self.co2Viscosity)
            else:
                co2_flow = 0.0

            #-----------------------------------------------------------------------
            # BRINE.

            # Compute the effective wet saturation for cell.
            effective_saturation = perm.compute_effective_saturation(
                base_co2_saturation, self.brineResSaturation,
                self.co2ResSaturation)

            # Get base capillary pressure (Pa) for cell.
            capillary_press = perm.compute_capillary_pressure(
                self.relativeModel, effective_saturation, seal_controls,
                self.entry)

            # Get effective brine pressure (MPa) for cell.
            base_pressure = perm.compute_brine_pressure(base_co2_pressure,
                                                        capillary_press)
            top_pressure = perm.compute_brine_pressure(top_brine_pressure,
                                                       capillary_press)

            # Get brine relative permeability for cell.
            relative_perm = perm.brine_relative_permeability(
                self.relativeModel, effective_saturation, seal_controls)

            # Get effective perm. in m2.
            effective_permeability = \
                perm.compute_current_permeability(relative_perm,
                                                  self.influence,
                                                  self.permeability)

            # Compute brine volume flux (m3/s) for cell.
            pressure_head = ((base_pressure - top_pressure)
                             / self.thickness)
            hydraulic_head = sunit.gravity() * self.brineDensity
            brine_flow = (self.area * effective_permeability
                          * (pressure_head - hydraulic_head)
                          / self.brineViscosity)

            # Add CO2 dissolved in Brine to flow - assuming brine 100% saturated.
            if base_co2_saturation > V_SMALL_SAT and brine_flow > 0.0:
                co2_flow += (self.co2Solubility * self.brineDensity * brine_flow
                             * sunit.co2_molar_mass() / (1000.0 * self.co2Density))

        else:
            # If not active, no CO2/brine flow
            co2_flow = 0.0
            brine_flow = 0.0

        return co2_flow, brine_flow


    def track_history(self, base_co2_pressure, co2_flow_rate, time_step):
        """ Increase time history of grid cells where flow occurs.
        Input Variables:
            base_co2_pressure = CO2 pressure at base of cell (Pa)
            co2_flow_rate = co2 flow through cell
            time_step = current time step (yrs)
         Return:
            None
        """
        # Flow occurs only in active cells and when pressure is large enough.
        if self.status == 1 and base_co2_pressure > self.entry:
            # Alteration occurs with a minimum flow / flow_rate
            if co2_flow_rate > self.flowMinimum:
                self.history += time_step

        # Return None


    @classmethod
    def assign_controls(cls, seal_controls):
        """ Assign cell class parameters from dictionary.
        Input Variable:
            seal_controls = dictionary of input values for seal
        """
        # Set class parameters

        # Fluid parameters.
        cls.brineDensity = seal_controls['brine_density']
        cls.brineViscosity = seal_controls['brine_viscosity']
        cls.co2Density = seal_controls['co2_density']
        cls.co2Viscosity = seal_controls['co2_viscosity']
        cls.co2Solubility = seal_controls['co2_solubility']

        # Two-phase model parameters.
        cls.relativeModel = seal_controls['relative_model']
        cls.brineResSaturation = seal_controls['resid_brine']
        cls.co2ResSaturation = seal_controls['resid_co2']

        # Time-model parameters.
        cls.model = seal_controls['model']
        if cls.model > 0:
            cls.totalEffect = seal_controls['total_effect']
            cls.rateEffect = seal_controls['time_rate']
            cls.reactivity = seal_controls['reactivity']
            cls.clayContent = seal_controls['clay_content']
            cls.carbonateContent = seal_controls['carbonate_content']
            cls.clayType = seal_controls['clay_type']

        # return None


def convert_flows(co2_flow, brine_flow, current_time, past_time, cols):
    """  Convert flows from one step (1D array) in flux in 2D format.
    Input Variables:
        co2_flow = CO2 flow (NumPy array)
        brine_flow =  (NumPy array)
        current_time = time for current step
        past_time = time for previous step
        number of columns
    Returns:
        co2_flow = corrected CO2 values
        brine_flow = corrected brine values
    """
    # For grid, convert seal vol. rates into mass flows for each cell.
    # -- Intervals are in years, rate is in seconds.
    interval = current_time - past_time
    co2_flow *= interval * sunit.yrs_to_seconds() * Cell.co2Density
    brine_flow *= interval * sunit.yrs_to_seconds() * Cell.brineDensity

    # Convert flows from kg to tonnes.
    co2_flow *= sunit.kilo_to_tonne()
    brine_flow *= sunit.kilo_to_tonne()

    # Convert flow array results from 1D to 2D.
    co2_flow.shape = (co2_flow.size//cols, cols)
    brine_flow.shape = (brine_flow.size//cols, cols)

    return (co2_flow, brine_flow)


##-----------------------------------------------------------------------------
## End of module
#--------1---------2---------3---------4---------5---------6---------7---------8
